const path = require('path');
const fs = require('fs'); 
const dir = './06_extracted_emails';

let countFiles=0;
let filename="";
let filenames=[];
let filePath="";
let obj;
let allMails=[];


fs.readdir(dir, (err, files) => {
  countFiles = (files.length);
  filenames = getFileNames(countFiles);
  for (let i=0; i < filenames.length; i++){
      processFile(filenames[i])
  }
  fs.writeFile('./06_extracted_emails/2022-03-01.json', JSON.stringify(allMails), function (err){if(err){throw err}});
  console.log(allMails.length);
});


function getFileNames(countFiles){
    for (let i = 0; i < countFiles; i++){
        filename = "mails_0" + (i+1).toString() + ".json"
        filenames[i]=filename.toString();
    }
    return filenames;
}

function processFile(filename){
    console.log("Called with: ", filename);
    filePath = path.join(__dirname, '/06_extracted_emails', filename);filename
    console.log("filepath is: ", filePath);
    obj = JSON.parse(fs.readFileSync(filePath, 'utf8'));
    obj.forEach(element => {
        allMails.push(element);
    });
    allMails = [ ...new Set(allMails)]; 
}