const path = require('path');
const fs = require('fs'); 
const domainPing = require("domain-ping");
const dir = './04_url_all';

let countFiles=0;
let filename="";
let filenames=[];
let filePath="";
let newPath="";
let obj;
let reached=[];
let pinged = [];
let finish=false;
let domain;

startPinging();

function startPinging(){
    fs.readdir(dir, (err, files) => {
    countFiles = (files.length);
    filenames = getFileNames(countFiles);
    for (let i=0; i < filenames.length; i++){
        finish = pingURL(filenames[i])
    }
    });
}

function getFileNames(countFiles){
    for (let i = 0; i < countFiles; i++){
        filename = "p" + i.toString() + ".json"
        filenames[i]=filename.toString();
    }
    console.log(filenames);
    return filenames;
}

function pingURL(filename){
    filePath = path.join(__dirname, '/04_url_all', filename);
    fs.appendFile('./received_log.json', "\n", function (err) {if (err) throw err});
    fs.appendFile('./received_log.json', JSON.stringify(filename), function (err) {if (err) throw err});
    obj = JSON.parse(fs.readFileSync(filePath, 'utf8'));
    pinged=callURL(obj)
    newPath = path.join(__dirname, '/05_reached_url', filename);
    fs.writeFile(newPath, JSON.stringify(pinged), function (err){if(err){throw err}});
    return true;
}

function callURL(urls){
    fs.appendFile('./received_log.json', "\n", function (err) {if (err) throw err});
    fs.appendFile('./received_log.json', JSON.stringify(urls.length), function (err) {if (err) throw err});
    for (let i=0; i < urls.length; i++){
        domain = urls[i].substring(4);
        domainPing(domain)
            .then((res) => {
                console.log(res);
                reached.push(res);
                fs.appendFile('./04b_ping_logs/ping_log.json', "\n", function (err) {if (err) throw err});
                fs.appendFile('./04b_ping_logs/ping_log.json', JSON.stringify(res), function (err) {if (err) throw err});
            }).catch((error) => {
                console.error(error);
                fs.appendFile('./04b_ping_logs/ping_log.json', "\n", function (err) {if (err) throw err});
                fs.appendFile('./04b_ping_logs/ping_log.json', JSON.stringify(error), function (err) {if (err) throw err});
            });   
    }
    return reached;
}