#!/bin/bash

FILES="./04b_ping_logs/*ping_log*"
touch ./05_reached_url/domains.json
for f in $FILES
do
  echo "Processing $f file..."
  grep "true" $f >> ./05_reached_url/response_list.txt
  echo "[" >> ./05_reached_url/domains.json
  cat ./05_reached_url/response_list.txt | cut -d "," -f1 | cut -d ":" -f2  | sed '$!s/$/,/' >> ./05_reached_url/domain.json
  echo "]" >> ./05_reached_url/domains.json
done