const path = require('path');
const fs = require('fs'); 
const dir = './03_processed';

let countFiles=0;
let filename="";
let filenames=[];
let filePath="";
let newPath="";
let obj;
let newWord = "";
let newWords = [];
let created;

fs.readdir(dir, (err, files) => {
  countFiles = (files.length);
  filenames = getFileNames(countFiles);
  for (let i=0; i < filenames.length; i++){
      createURL(filenames[i])
  }
});

function getFileNames(countFiles){
    for (let i = 0; i < countFiles; i++){
        filename = "p" + (i+1).toString() + ".json"
        filenames[i]=filename.toString();
    }
    return filenames;
}

function createURL(filename){
    filePath = path.join(__dirname, '/03_processed', filename);
    obj = JSON.parse(fs.readFileSync(filePath, 'utf8'));
    created = composeComponents(obj);
    newPath = path.join(__dirname, '/04_url_all', filename);
    fs.writeFile(newPath, JSON.stringify(created), function err() {if(err){throw err}});
    newWords=[];
}

function composeComponents(object){
    object.forEach(word => {
        newWord="www." + word + ".ru";
        newWords.push(newWord);
    });
    return newWords;
}