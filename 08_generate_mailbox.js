const fs = require('fs'); 

const names=[];
const nicknames=[];
const surnames=[];
const words=[];
const dirb = [];

const specialChars=[".","-","_",""];

const testSet = {
    names: ["Ivan, Alexander"],
    nicknames: ["Sasha"],
    surnames: ["Radulov", "Aremova"],
    words: ["molodec"],
    dirb: ["dirb1", "dirb2"]
}

function generateEmails(){
    const nameSurname = generateDuo(testSet.names, testSet.surnames);
    console.log(nameSurname);
}

function generateDuo(first, second){
    let results = []
    let ending = "@yandex.com"
    first.forEach(f =>{
        second.forEach(s => {
            specialChars.forEach(ch =>{
                results.push(f + ch + s + ending, s + ch + f + ending);
            })
        })
    })
}