const fs = require('fs'); 

let rawdata = fs.readFileSync('./05_reached_url/domains.json');
let urls = JSON.parse(rawdata);
let newUrls = [];
let mailList = [];
let finalList = [];

url_test=["https://raund.ru/", "https://profisystems.cz"]

urls.forEach(url => {
    url = "https://" + url;
    newUrls.push(url);
});

const getScript = (url) => {
    return new Promise((resolve, reject) => {
        const http      = require('http'),
            https     = require('https');

        let client = http;

        if(url === 'undefined'){
            resolve();
        }
        if (url.toString().indexOf("https") === 0) {
            client = https;
        }

        client.get(url, (resp) => {
            let data = '';

            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            });

            // The whole response has been received. Print out the result.
            resp.on('end', () => {
                resolve(data);
            });

        }).on("error", (err) => {
            resolve();
        });
    });
};
const pickMails = (data) => {
    return new Promise((resolve, reject)=> {
        if(data === "undefined"){resolve();}
        const webData = data.toString();
        const re = /(?:[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/gi;
        const result = webData.match(re);
        const unique = [ ...new Set(result)]; 
        resolve(unique);
    });
};
const pickLinks = (data, url) => {
    return new Promise((resolve, reject)=> {
        if ((data === "undefined") || (url === "undefined")){resolve();}
        const webData = data.toString();
        const re = /href=\"(.*?)\"/gi;
        const result = webData.match(re);
        const unique = [ ...new Set(result)]; 
        const noNoise = [] 
        unique.forEach(link => {
            if((link === "") || (link === "undefined")){resolve();}
            str = link.toString();
            if(str.match(/\.[ps][nv]g/gi)){
            } else if((str.match(/fonts/gi)) ||(str.match(/manifest/gi)) || (str.match(/mailto/gi)) || (str.match(/css/gi))){
            } 
            else {
                str = str.substring(5);
                str = str.replace(/[\"#]/g,'');
                if(str.length > 0){
                    let newUrl;
                    if((str.match("http:")) || (str.match("https:"))){
                        newUrl = str;
                    } else {
                        newUrl = url + "/" + str;
                    }
                    noNoise.push(newUrl);
                }
            }
        });
        resolve(noNoise);
    });
};
for (let i=0; i< newUrls.length; i++){
    (async (url) => {
        const collectedData = await getScript(url);
        if(collectedData){
            const emails = await pickMails(collectedData);
            mailList.push(emails);
            const links = await pickLinks(collectedData, url);
            if(links.length>0){
                fs.appendFile('./05_reached_url/url_extended.json', JSON.stringify(links), function (err) {if (err) throw err});
                fs.appendFile('./05_reached_url/url_extended.json', "\n", function (err) {if (err) throw err});
                console.log(`[${i}]_${links}`);
            }
            for(let j=0; j < links.length; j++){
                const newData = await(getScript(links[j]));
                const newMails = await pickMails(newData);
                mailList.push(newMails);
            };
        }
        if(mailList.length>0){
            mailList.forEach(mail => {
                if (mail.length < 1){
                }
                else {
                    finalList.push(mail[0]);
                }
            })
            const removeDuplicates = [ ...new Set(finalList)];
            fs.writeFile('./06_extracted_emails/mails.json', JSON.stringify(removeDuplicates), function (err){if(err){throw err}});
        }
    })(newUrls[i]);
}



