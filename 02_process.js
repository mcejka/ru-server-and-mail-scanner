const path = require('path');
const fs = require('fs'); 
const dir = './02_transliterated';

const ascii = /^[ -~]+$/;

let countFiles=0;
let filename="";
let filenames=[];
let filePath="";
let newPath="";
let obj;
let word = "";
let newWord = "";
let newWords = [];
let specialCharacters=[];
let special = "";
let newSpecial = true;


fs.readdir(dir, (err, files) => {
  countFiles = (files.length);
  filenames = getFileNames(countFiles);
  for (let i=0; i < filenames.length; i++){
      processFile(filenames[i])
  }
});


function getFileNames(countFiles){
    for (let i = 0; i < countFiles; i++){
        filename = "p" + (i+1).toString() + ".json"
        filenames[i]=filename.toString();
    }
    return filenames;
}

function processFile(filename){
    //console.log("Processing: ", filename);
    filePath = path.join(__dirname, '/02_transliterated', filename);
    obj = JSON.parse(fs.readFileSync(filePath, 'utf8'));
    processed = substitudeCharacters(obj);
    newPath = path.join(__dirname, '/03_processed', filename);
    fs.writeFile(newPath, JSON.stringify(processed), function err() {if(err){throw err}});
    newWords=[];
}

function substitudeCharacters(object){
    for (let i = 0; i < object.length; i++){
       word = object[i]
       for (let j = 0; j < word.length; j++){
        if ( !ascii.test( word[j] ) ) {
            special = word[j];
            newSpecial = true;
            for(let k = 0; k < specialCharacters.length; k++){
                if(special === specialCharacters[k]){
                    newSpecial = false;
                }
            }
            if(newSpecial == true){
                specialCharacters.push(special)
            }
        }
       } 
    }
    object.forEach(word => {
        regE = /[Èè]/gi;
        regS = /[šŝŠŜ]/gi
        regZ = /[žŽ]/gi
        regC = /[Čč]/gi
        regO = /[ʹʺ]/gi

        word=word.replace(regE, 'e');
        word=word.replace(regS, 's');
        word=word.replace(regZ, 'z');
        word=word.replace(regC, 'c');
        word=word.replace(regO, '');
        newWord=word;
        newWords.push(newWord);
    });
    return newWords;
}